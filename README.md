# Financial-Models
From book Analyzing Financial Data and Implementing Financial Models Using R
By Clifford S. Ang

Contents
- [ ] Chapter 1. Prices
- [ ] Chapter 2. Individual Security Returns
- [ ] Chapter 3. Portfolio Returns
- [ ] Chapter 4. Risk
- [ ] Chapter 5. Factor Models
- [ ] Chapter 6. Risk-Adjusted Portfolio Performance Measures
- [ ] Chapter 7. Markowitz Mean-Variance Optimization
- [ ] Chapter 8. Fixed Income
- [ ] Chapter 9. Options
